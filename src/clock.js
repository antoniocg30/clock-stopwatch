(function(){
	var updateHours = function(){
		var date = new Date(),
		hours = date.getHours(),
		ampm,
		minutes = date.getMinutes(),
		seconds = date.getSeconds();

		var pHours = document.getElementById('hours'),
		pAMPM = document.getElementById('ampm'),
		pMinutes = document.getElementById('minutes'),
		pSeconds = document.getElementById('seconds');

		if (hours >= 12) {
			hours = hours - 12;
			ampm = 'PM';
		} else {
			ampm = 'AM';
		}

		if (hours == 0) {
			hours = 12;
		};

		pHours.textContent = hours;
		pAMPM.textContent = ampm;

		if (minutes < 10) {
			minutes = "0" + minutes
		};

		if (seconds < 10) {
			seconds = "0" + seconds
		};

		pMinutes.textContent = minutes;
		pSeconds.textContent = seconds;
	};

	updateHours();
	var interval = setInterval(updateHours, 1000);

}())